﻿using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;
using System.Collections;

public class PlayerAction : MonoBehaviour
{
    public GameObject scopeOverlay;
    public GameObject playerCamera;
    public Camera mainCamera;

    public float scopeFOV = 15f;

    private Vector2 m_Input;
    private bool isSniper;
    private float normalFOV = 30f;

    // Start is called before the first frame update
    void Start()
    {
        Actions actions = GetComponent<Actions>();
        PlayerController playerController = GetComponent<PlayerController>();

        playerController.SetArsenal("BasicRifle");

        normalFOV = mainCamera.fieldOfView;

    }

    // Update is called once per frame
    void Update()
    {
        Actions actions = GetComponent<Actions>();
        PlayerController playerController = GetComponent<PlayerController>();

        // Read input
        float horizontal = CrossPlatformInputManager.GetAxis("Horizontal");
        float vertical = CrossPlatformInputManager.GetAxis("Vertical");
        bool run = CrossPlatformInputManager.GetButton("Run");
        bool sitting = CrossPlatformInputManager.GetButton("Sitting");
        bool shoot = CrossPlatformInputManager.GetButton("Fire1");
        bool aiming = CrossPlatformInputManager.GetButton("Fire2");
        bool jump = CrossPlatformInputManager.GetButtonDown("Jump");

        

        if (isSniper)
        {   
            if (CrossPlatformInputManager.GetButtonDown("Fire2"))
                StartCoroutine(OnScoped());
            
            if (CrossPlatformInputManager.GetButtonUp("Fire2"))
                OnUnscoped();   
        }

        void OnUnscoped()
        {
            scopeOverlay.SetActive(false);
            playerCamera.SetActive(true);
            mainCamera.fieldOfView = normalFOV;
            Debug.Log(normalFOV);
        }

        IEnumerator OnScoped()
        {
            yield return new WaitForSeconds(.15f);
            scopeOverlay.SetActive(true);
            playerCamera.SetActive(false);
            mainCamera.fieldOfView = 10;
        }

    }
}
