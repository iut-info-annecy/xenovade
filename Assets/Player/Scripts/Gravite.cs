﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;
using UnityStandardAssets.CrossPlatformInput;

public class Gravite : MonoBehaviour
{
    public FirstPersonController FirstPersonController;
    public float cooldown = 6f;
    public const float timeForRotate = 1f;

    public GameObject templeCollider;
    public GameObject templeCeilingCollider;

    private float rotateTimeLeft;
    private float rotationDone;
    private bool shouldRotate = false;

    private bool shouldWait = false;
    private float waitDone;

    private bool button;
    private bool isInTemple = false;

    // Update is called once per frame
    void Update()
    {
        if (isInTemple)
        {
            if (shouldRotate)
            {
                float nextRotation = (180f / timeForRotate) * Time.deltaTime;
                if (rotationDone + nextRotation > 180)
                { 
                    FirstPersonController.transform.rotation = new Quaternion(0, 0,
                        (FirstPersonController.m_GravityMultiplier < 0) ? 180 : 0,
                    0);
                    FirstPersonController.Start();
                    FirstPersonController.FixedUpdate();
                    shouldRotate = false;
                }
                else
                {
                    FirstPersonController.transform.Rotate(0, 0, nextRotation);
                    FirstPersonController.Start();
                    FirstPersonController.FixedUpdate();
                    rotationDone += nextRotation;
                }
            } 

            if(shouldWait)
            {
                float needToWait = (100f / cooldown) * Time.deltaTime;
            
                waitDone += needToWait;

                if(waitDone > 100)
                {
                    shouldWait = false;
                }
                Debug.Log("waitDone : " + waitDone);
            }

            if (CrossPlatformInputManager.GetButtonDown("ChangeGravity") && !shouldWait)
            {
                inverseGravite();
                waitDone = 0;
                shouldWait = true;
            }
        }
        else
        {
            shouldWait = false;
            shouldRotate = false;
        }
    }

    private void OnTriggerEnter(Collider templeCollider)
    {
        isInTemple = true;
    }

    private void OnTriggerExit(Collider templeCollider)
    {
        isInTemple = false;
    }

    void inverseGravite()
    {
        FirstPersonController.m_GravityMultiplier = FirstPersonController.m_GravityMultiplier * -1;
        FirstPersonController.needToJumpForGravite = true;

        FirstPersonController.FixedUpdate();
        FirstPersonController.Update();

        shouldRotate = true;
        rotationDone = 0;
    }
}
