﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraPosition : MonoBehaviour
{
    public Camera fpscam;
    public Transform head;

    // Start is called before the first frame update
    void Start()
    {
        fpscam.transform.position = head.position;
    }
}
