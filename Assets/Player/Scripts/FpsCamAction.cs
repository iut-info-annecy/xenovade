﻿using System.Collections;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;
using UnityStandardAssets.Characters.FirstPerson;

public class FpsCamAction : MonoBehaviour
{
    public FirstPersonController FPC;

    public Animator animatorSniper;
    public Animator animatorHeavy;

    public GameObject rifleCamera;
    public GameObject pistolCamera;
    public GameObject heavyCamera;
    public GameObject sniperCamera;

    public GameObject CamPlayer;
    public GameObject CamRifle;
    public GameObject CamPistol;
    public GameObject CamHeavy;
    public GameObject CamSniper;

    public Camera mainCamera;

    public GameObject selectWeapon;
    public GameObject echapMenu;

    public GameObject scopeSniperOverlay;
    public GameObject crossOverlay;

    private bool RifleIsScoped = false;
    private bool SniperIsScoped = false;
    private bool HeavyIsScoped = false;
    private bool PistolIsScoped = false;

    private Vector2 m_Input;
    public PlayerController playerController;
    public Actions actions;
    public FirstPersonController firstPersonController;

    private float normalFOV = 60f;
    private float pistolFOV = 30f;
    private float rifleFOV = 30f;
    private float heavyFOV = 30f;
    private float sniperFOV = 15f;

    float horizontal;
    float vertical;
    bool run;
    bool sitting;
    bool shoot;
    bool aiming;
    bool jump;
    bool echap;
    bool afficheEchap = false;

    private void Start()
    {
        playerController.SetArsenal("Sniper");
        localPlayer();
    }

    void Update()
    {
        readInput();
        changeArme();
        actionPlayer();
        activeScopeOverlay();
        afficheMenu();
    }

    void afficheMenu()
    {
        if (echap)
        {
            afficheEchap = !afficheEchap;
            echapMenu.SetActive(afficheEchap);
            firstPersonController.enabled = !afficheEchap;
        }
    }

    public void cliqueSurChangeWeapon()
    {
        Debug.Log("changeWeapon");
        afficheEchap = false;
        echapMenu.SetActive(false);
        selectWeapon.SetActive(true);
    }

    private void localPlayer()
    {
        if (FPC.IsLocalPlayer)
        {
            CamPlayer.gameObject.SetActive(false);
            CamRifle.gameObject.SetActive(false);
            CamPistol.gameObject.SetActive(false);
            CamHeavy.gameObject.SetActive(false);
            CamSniper.gameObject.SetActive(false);
        }
    }





















    void readInput()
    {
        horizontal = CrossPlatformInputManager.GetAxis("Horizontal");
        vertical = CrossPlatformInputManager.GetAxis("Vertical");
        run = CrossPlatformInputManager.GetButton("Run");
        sitting = CrossPlatformInputManager.GetButton("Sitting");
        shoot = CrossPlatformInputManager.GetButton("Fire1");
        aiming = CrossPlatformInputManager.GetButton("Fire2");
        jump = CrossPlatformInputManager.GetButtonDown("Jump");
        echap = CrossPlatformInputManager.GetButtonDown("Cancel");
    }

    void changeArme()
    {
        if (Input.GetKey(KeyCode.F2))
        {
            playerController.SetArsenal("Pistol");
            changeAllStateOffCameraWeapon(false);
            pistolCamera.SetActive(true);
            crossOverlay.SetActive(true);
        }
        if (Input.GetKey(KeyCode.F3))
        {
            playerController.SetArsenal("Rifle");
            changeAllStateOffCameraWeapon(false);
            rifleCamera.SetActive(true);
            crossOverlay.SetActive(true);
        }
        if (Input.GetKey(KeyCode.F4))
        {
            playerController.SetArsenal("Heavy");
            changeAllStateOffCameraWeapon(false);
            heavyCamera.SetActive(true);
            crossOverlay.SetActive(false);
        }
        if (Input.GetKey(KeyCode.F5))
        {
            playerController.SetArsenal("Sniper");
            changeAllStateOffCameraWeapon(false);
            sniperCamera.SetActive(true);
            crossOverlay.SetActive(false);
        }
    }

    void changeAllStateOffCameraWeapon(bool state)
    {
        rifleCamera.SetActive(state);
        pistolCamera.SetActive(state);
        heavyCamera.SetActive(state);
        sniperCamera.SetActive(state);
    }



    void actionPlayer()
    {
        m_Input = new Vector2(horizontal, vertical);

        if (m_Input.sqrMagnitude > 1)
        {
            m_Input.Normalize();
        }

        if (m_Input.y != 0 && !aiming)
        {
            if (run)
                actions.Run();
            else
                actions.Walk();
        }
        else
            actions.Stay();

        if (aiming)
            actions.Aiming();

        if (sitting)
        {
            if (!actions.isSitting())
            {
                actions.Sitting();
            }
        }
        else
        {
            if (actions.isSitting())
            {
                actions.Sitting();
                actions.Stay();
            }
        }

        if (jump)
        {
            if (actions.isSitting())
                actions.Sitting();
            else
            {
                actions.Jump();
            }

        }

        if (shoot && aiming)
        {
            actions.Attack();
            // Effectuer le tire
        }

        /*if (false)// S'il ce fait tirer dessus
        {
            actions.Damage();
        }*/
    }

    void activeScopeOverlay()
    {
        string arsenal = playerController.GetArsenal();

        if (CrossPlatformInputManager.GetButtonDown("Fire2"))
            StartCoroutine(OnScoped());

        if (CrossPlatformInputManager.GetButtonUp("Fire2"))
            OnUnscoped();
    }

    void OnUnscoped()
    {
        string arsenal = playerController.GetArsenal();

        scopeSniperOverlay.SetActive(false);

        if (arsenal == "Heavy")
        {
            animatorHeavy.SetBool("HeavyScoped", false);
        }

        if (arsenal == "Sniper")
        {
            sniperCamera.SetActive(true);
        }



        mainCamera.fieldOfView = normalFOV;
    }

    IEnumerator OnScoped()
    {
        string arsenal = playerController.GetArsenal();

        if (arsenal == "Sniper")
        {
            animatorSniper.SetBool(arsenal + "Scoped", true);
        }

        if (arsenal == "Heavy")
        {
            animatorHeavy.SetBool(arsenal + "Scoped", true);
        }

        yield return new WaitForSeconds(.20f);

        if (CrossPlatformInputManager.GetButton("Fire2"))
        {
            if (arsenal == "Sniper")
            {
                animatorSniper.SetBool(arsenal + "Scoped", true);
                scopeSniperOverlay.SetActive(true);
                mainCamera.fieldOfView = sniperFOV;
                sniperCamera.SetActive(false);
            }
        }
        else
        {
            OnUnscoped();
        }
    }

}
