﻿using UnityEngine;
using System.Collections;
using UnityEngine.Rendering;

[RequireComponent(typeof(Animator))]
public class PlayerController : MonoBehaviour
{

    public Transform rightGunBone;
    public Transform leftGunBone;
    public Arsenal[] arsenal;
    public Camera fpscam;
    public Transform decallageVisePisotlet;
    public Transform decallageViseSniper;

    private Animator animator;
    private string arsenalActual;

    void Awake()
    {
        animator = GetComponent<Animator>();
        if (arsenal.Length > 0)
            SetArsenal(arsenal[0].name);
    }

    public void SetArsenal(string name)
    {
        foreach (Arsenal hand in arsenal)
        {
            if (hand.name == name)
            {
                if (rightGunBone.childCount > 0)
                    Destroy(rightGunBone.GetChild(0).gameObject);
                if (leftGunBone.childCount > 0)
                    Destroy(leftGunBone.GetChild(0).gameObject);
                if (hand.rightGun != null)
                {
                    GameObject newRightGun = (GameObject)Instantiate(hand.rightGun);
                    newRightGun.transform.parent = rightGunBone;
                    newRightGun.transform.localPosition = Vector3.zero;
                    newRightGun.transform.localRotation = Quaternion.Euler(90, 0, 0);
                    newRightGun.transform.localPosition = hand.decalage.position;
                    newRightGun.layer = hand.layerNumber;
                    foreach (Transform child in newRightGun.transform.GetComponentsInChildren<Transform>())
                    {
                        child.gameObject.layer = hand.layerNumber;
                    }
                }
                if (hand.leftGun != null)
                {
                    GameObject newLeftGun = (GameObject)Instantiate(hand.leftGun);
                    newLeftGun.transform.parent = leftGunBone;
                    newLeftGun.transform.localPosition = Vector3.zero;
                    newLeftGun.transform.localRotation = Quaternion.Euler(90, 0, 0);
                }
                animator.runtimeAnimatorController = hand.controller;

                if (hand.controller.name == "OnePistolController")
                {
                    fpscam.transform.localPosition = decallageVisePisotlet.position;
                }

                if (hand.controller.name == "SniperRifleController")
                {
                    fpscam.transform.localPosition = decallageViseSniper.position;
                }
                
                arsenalActual = hand.name;

                return;
            }
        }
    }

    public string GetArsenal()
    {
        return arsenalActual;
    }

    [System.Serializable]
    public struct Arsenal
    {
        public string name;
        public GameObject rightGun;
        public GameObject leftGun;
        public RuntimeAnimatorController controller;
        public Transform decalage;
        public int layerNumber;
    }
}
