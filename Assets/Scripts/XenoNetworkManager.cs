﻿using UnityEngine;
using Mirror;
using UnityStandardAssets.Characters.FirstPerson;

public class CustomParameters : MessageBase {
    // use whatever credentials make sense for your game
    // for example,  you might want to pass the accessToken if using oauth
    public string team;
    public string networkMode;
    public string IPAdress;
    public string name;
}

public class XenoNetworkManager : NetworkManager {
    public GameManager gameManager;

    //private bool isFirstPlayer = true;

    public override void OnStartServer() {
        //gameManager = new GameManager();
    }

    public override void OnServerConnect(NetworkConnection conn) {
        if (!gameManager.AddPlayer(conn)) {
            // TODO: Handle spectating here if necessary
            // NOTE: Spectating also means waitlist
        }
    }

    public override void OnServerDisconnect(NetworkConnection conn) {
        gameManager.RemovePlayer(conn);
    }

    public override void OnServerAddPlayer(NetworkConnection conn, AddPlayerMessage extraMessage) {
        /*Transform startPos = GetStartPosition();
        GameObject player = startPos != null
            ? Instantiate(playerPrefab, startPos.position, startPos.rotation)
            : Instantiate(playerPrefab);
        
        NetworkServer.AddPlayerForConnection(conn, player);*/

        if (extraMessage.value == null) {
            gameManager.AddPlayer(conn);
            return;
        }

        CustomParameters customParameters = MessagePacker.Unpack<CustomParameters>(extraMessage.value);
        gameManager.AddPlayer(conn, customParameters);
    }

    public override void OnServerRemovePlayer(NetworkConnection conn, NetworkIdentity player) {
        gameManager.NotifyPlayerDied(conn);
        NetworkServer.Destroy(player.gameObject);
    }

    public override void OnClientConnect(NetworkConnection conn) {
        base.OnClientConnect(conn);

        Parameters parameters = null;//GameObject.FindGameObjectWithTag("Parameters").GetComponent<Parameters>();

        CustomParameters customParameters = null;/*new CustomParameters() {
            team = parameters.preferredTeam.ToString(),
            networkMode = parameters.chosenNetworkMode.ToString(),
            IPAdress = parameters.IPAdress,
            name = parameters.name
        };*/

        if (parameters != null)
            ClientScene.AddPlayer(conn, MessagePacker.Pack(customParameters));
        else {
            ClientScene.AddPlayer(conn);
        }
    }
}