﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScriptPaulETHAN : MonoBehaviour
{
    //Vitesses de déplacement
    public float walkSpeed;
    public float runSpeed;
    public float turnSpeed;

    //Inputs
    public KeyCode inputFront;
    public KeyCode inputBack;
    public KeyCode inputLeft;
    public KeyCode inputRight;

    public Vector3 jumpSpeed;
    CapsuleCollider playerCollider;
    
    void Start()
    {
        playerCollider = gameObject.GetComponent<CapsuleCollider>();
    }
    
    void Update()
    {
        // Si on avance
        if (Input.GetKey(inputFront))
        {
            transform.Translate(0, 0, walkSpeed * Time.deltaTime);
        }

        // Si on sprint
        if (Input.GetKey(inputFront) && Input.GetKey(KeyCode.LeftShift))
        {
            transform.Translate(0, 0, runSpeed * Time.deltaTime);
        }

        // Si on recule
        if (Input.GetKey(inputBack))
        {
            transform.Translate(0, 0, -(walkSpeed / 2) * Time.deltaTime);
        }

        // Rotation à gauche
        if (Input.GetKey(inputLeft))
        {
            transform.Rotate(0, -turnSpeed * Time.deltaTime, 0);
        }

        // Rotation à droite
        if (Input.GetKey(inputRight))
        {
            transform.Rotate(0, turnSpeed * Time.deltaTime, 0);
        }
    }
}
