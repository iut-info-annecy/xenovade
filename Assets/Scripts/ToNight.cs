﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToNight : MonoBehaviour
{
    // Interpolate light color between two colors back and forth
    float duration = 15.0f;
    Color color0 = Color.white;
    Color color1 = Color.black;

    Light lt;

    public bool shouldWait = false;
    private float waitDone;

    void Start()
    {
        lt = GetComponent<Light>();
    }

    void Update()
    {
        // set light color
        float t = 0;

        if (shouldWait)
        {
            float needToWait = (1f / duration) * Time.deltaTime;

            waitDone += needToWait;
            t = waitDone;
            lt.color = Color.Lerp(color0, color1, t);

            if (waitDone >= 1)
            {
                shouldWait = false;
            }
            Debug.Log("waitDone : " + waitDone);
        }

        if (!shouldWait)
        {
            waitDone = 0;
        }

    }
}
