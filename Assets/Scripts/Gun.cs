﻿using UnityEngine;
using Mirror;

public class Gun : NetworkBehaviour {
    public float damage = 30f;
    public float range = 10000f;
    public float frequence = 1f / 2f;
    public float recul = 1f;
    public float maxAmmo = 20f;

    public AudioClip shoot_sound;
    public AudioClip recharge_sound;
    public GameObject impactEffect;

    private AudioSource audioSource;

    private float secRemaining = 0;
    private float ammo;

    public Camera fpsCam;

    void Start()
    {
        audioSource = GetComponent<AudioSource>();
        fpsCam = gameObject.GetComponentInChildren<FpsCamAction>().gameObject.GetComponent<Camera>();
        
        Recharger();
    }

    // Update is called once per frame
    void Update() {
        if (isLocalPlayer)
        {
                if (secRemaining > 0)
            {
                secRemaining-=Time.deltaTime;
            }
            if (Input.GetButton("Fire1") && secRemaining <= 0 && ammo > 0)
            {
                CmdShoot(fpsCam.transform.position + fpsCam.transform.forward, fpsCam.transform.forward, range);

                ExplosionHandler eh = GetComponent<ExplosionHandler>();
                eh.Explosion(-fpsCam.transform.forward, recul, 0.05f);

                secRemaining = frequence;
               ammo--;
            }
            if (Input.GetButtonDown("Fire3"))
            {
                Recharger();
                audioSource.clip = recharge_sound;
                if (audioSource != null && audioSource.enabled)
                    audioSource.Play();
            }
        }
    }

    void Recharger()
    {
        ammo = maxAmmo;
    }

    [Command]
    void CmdShoot(Vector3 position, Vector3 direction, float range)
    {
        ShotSound();
        RpcSendSoundToPlayers();

        RaycastHit hit;
        if (Physics.Raycast(position + direction, direction, out hit, range))
        {
            Debug.Log("Someone touched " + hit.transform.gameObject.tag);
            Target target = hit.transform.GetComponent<Target>();
            Grenade grenade = hit.transform.GetComponent<Grenade>();

            ImpactParticles(hit.point, hit.normal);
            RpcImpactParticles(hit.point, hit.normal);

            if (target != null)
            {
                TargetTakeDamage(target.GetComponent<NetworkIdentity>().connectionToClient, target.GetComponent<Player>().gameObject, 50);
                //target.TargetKnockback(target.GetComponent<NetworkIdentity>().connectionToClient, recul);
            }

            if (grenade != null)
            {
                grenade.Explode();
            }
        }
    }

    [TargetRpc]
    private void TargetTakeDamage(NetworkConnection targetConnection, GameObject player, float calculatedDamage)
    {
        player.GetComponent<Target>().TakeDamage(calculatedDamage);
    }
    
    private void ShotSound()
    {
        audioSource.clip = shoot_sound;
        if (audioSource != null && audioSource.enabled)
            audioSource.Play();
    }

    [ClientRpc]
    private void RpcSendSoundToPlayers()
    {
        ShotSound();
    }

    private void ImpactParticles(Vector3 hitPoint, Vector3 hitRotation)
    {
        Instantiate(impactEffect, hitPoint, Quaternion.LookRotation(hitRotation));
    }

    [ClientRpc]
    private void RpcImpactParticles(Vector3 hitPoint, Vector3 hitRotation)
    {
        ImpactParticles(hitPoint, hitRotation);
    }
}