﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;
using UnityEngine.UI;
using Random = System.Random;

public class GameManager : NetworkBehaviour {

    public GameObject playerPrefab;
    public Button startButton;
    public List<GameObject> spawnAttackers = new List<GameObject>();
    public List<GameObject> spawnDefenders = new List<GameObject>();
    public List<Capturable> controlPoints = new List<Capturable>();
    public Text textProgress;
    public GameObject doorTop, doorBottom;
    public Text textTimeRemaining;
    public float timeRemaining = 10 * 60f;

    public ClientGameManager clientGameManager;
    public XenoNetworkManager manager;
    public ToNight toNight;
    public ToNightAreaLight toNightAreaLight;

    ///////////////////////////////////////

    private const int maxPlayers = 6;
    private const float respawnTime = 5f; // Seconds
    private int currentZone = 1;

    private float timeToCapture;
    private float percentage;

    private List<NetworkConnection> playerConnections;
    private List<NetworkConnection> spectatorConnections;

    private Dictionary<NetworkConnection, CustomParameters> customParameters;

    private Dictionary<NetworkConnection, GameObject> attackers;
    private Dictionary<NetworkConnection, GameObject> defenders;

    private float captureTimeLeft;
    private String captureProgressText = "capture";

    private void Awake() {
        playerConnections = new List<NetworkConnection>();
        spectatorConnections = new List<NetworkConnection>();
        
        customParameters = new Dictionary<NetworkConnection, CustomParameters>();
        
        attackers = new Dictionary<NetworkConnection, GameObject>();
        defenders = new Dictionary<NetworkConnection, GameObject>();

        captureTimeLeft = controlPoints[currentZone - 1].timeToCapture;
        
        captureProgressText = textProgress.text;
        textProgress.text = captureProgressText + " 0%";
    }

    private void Start() {
        Debug.Log("OnStart called.");
        //GameObject.FindGameObjectWithTag("Parameters").GetComponent<Parameters>().Apply(manager);
    }

    private void Update() {
        if (timeRemaining <= 0) {
            textTimeRemaining.text = "Victoire des défenseurs !";
        }
        else {
            timeRemaining -= Time.deltaTime;
            textTimeRemaining.text = "Time remaining: " + ((int) (timeRemaining / 60)) + " min and " +
                                     ((int) timeRemaining % 60) + " sec";
        }
        clientGameManager.RpcSetTextProgress(textTimeRemaining.text);

        // Update capture points
        if (captureTimeLeft <= 0f) {
            if (controlPoints.Count > currentZone + 1) {
                if (currentZone == 1) {
                    // Switch to night
                    toNight.shouldWait = true;
                    toNightAreaLight.shouldWait = true;

                    clientGameManager.RpcSwitchToNight();
                }
                else
                {
                    doorTop.transform.localPosition = new Vector3(0, 19.72519f + 6.27481f, 0);
                    doorBottom.transform.localPosition = new Vector3(0, -15, 0);
                }
                Debug.Log("Code ultra moche à modifier quand y'aura le temps");
                
                currentZone++;
                captureTimeLeft = controlPoints[currentZone - 1].timeToCapture;

                clientGameManager.RpcNextZone(controlPoints[currentZone - 1].timeToCapture);
            }
            else {
                textTimeRemaining.text = "Victoire des attaquants !";
                clientGameManager.RpcSetTextProgress(textTimeRemaining.text);
                return;
            }
        }

        bool attackerFound = false;
        bool defenderFound = false;
        foreach (Collider collider in controlPoints[this.currentZone - 1].colliders) {
            NetworkIdentity networkIdentity = collider.GetComponent<NetworkIdentity>();
            if (networkIdentity != null && networkIdentity.isClient) {
                if (attackers.ContainsKey(networkIdentity.connectionToClient))
                    attackerFound = true;
                else if (defenders.ContainsKey(networkIdentity.connectionToClient))
                    defenderFound = true;
            }
        }

        if (attackerFound && !defenderFound)
            captureTimeLeft -= Time.deltaTime;
        else
            return;
        
        timeToCapture = controlPoints[currentZone - 1].timeToCapture;
        percentage = (timeToCapture - captureTimeLeft) / timeToCapture;

        clientGameManager.RpcSetTimeToCaptureAndPercentage(timeToCapture, percentage);
        
        // Open door if necessary
        if (currentZone == 1) {
            doorTop.transform.localPosition = new Vector3(0, 19.72519f + 6.27481f * percentage, 0);
            doorBottom.transform.localPosition = new Vector3(0, -15 * percentage, 0);
        }

        // Update UI text
        String newProgressText = (char) currentZone + captureProgressText + " " +
                            Math.Floor(percentage * 100) + "%";
        clientGameManager.RpcUpdateUICaptureProgress(newProgressText);
    }
    

    public bool AddPlayer(NetworkConnection connection) => AddPlayer(connection, null);

    /// <summary>
    ///  Add a new player to the game by adding its connection
    /// </summary>
    /// <param name="connection">The new player's connection</param>
    /// <returns>true if the player was successfully added, false otherwise if added as spectator</returns>
    public bool AddPlayer(NetworkConnection connection, CustomParameters parameters) {
        customParameters[connection] = parameters;
        
        if (playerConnections.Count == maxPlayers) {
            if (spectatorConnections.IndexOf(connection) == -1)
                spectatorConnections.Add(connection);
            return false;
        }
        
        if (playerConnections.IndexOf(connection) == -1)
            playerConnections.Add(connection);
        return true;
    }

    public void RemovePlayer(NetworkConnection connection) {
        int index = playerConnections.IndexOf(connection);
        if (index != -1) {
            // Two bools because the player can be in no team (lobby for ex.)
            bool wasAttacker = attackers.ContainsKey(connection);
            bool wasDefender = defenders.ContainsKey(connection);

            if (!playerConnections.Remove(connection)) {
                Debug.LogError("Error deleting player connection");
                throw new Exception("Error deleting player connection");
            }

            if (wasAttacker)
                attackers.Remove(connection);
            else if (wasDefender)
                defenders.Remove(connection);

            // If at least one spectator, move it to the active players
            if (spectatorConnections.Count >= 1) {
                playerConnections.Add(spectatorConnections[0]);
                spectatorConnections.RemoveAt(0);

                if (wasAttacker || wasDefender) {
                    // Spawn player
                    GameObject playerObject = SpawnPlayer(wasAttacker);

                    // If 5 players are playing, we want 3 attackers and 2 defenders
                    if (wasAttacker) {
                        attackers.Add(spectatorConnections[0], playerObject);
                    }
                    else {
                        defenders.Add(spectatorConnections[0], playerObject);
                    }
                
                    NetworkServer.AddPlayerForConnection(spectatorConnections[0], playerObject);
                }
            }
        }

        // Works even if connection is not in the array
        spectatorConnections.Remove(connection);
    }

    /// <summary>
    /// Launches the game and randomizes the player's colors if necessary
    /// </summary>
    public void StartGame() {
        if (playerConnections.Count == 0) {
            Debug.Log("Not enough players to start game! " + playerConnections.Count + " players are currently connected.");
            return;
        }
        
        // Internal checks
        if (spawnAttackers.Count != controlPoints.Count - 1 || spawnDefenders.Count != controlPoints.Count - 1) {
            throw new Exception("Error! The number of spawns in SpawnAttackers (currently " +
                                spawnAttackers.Count + ") should be equal to the one of SpawnDefenders (currently " +
                                spawnDefenders.Count + ") and equal to capturePoints' length + 1 in the GameManager (currently " +
                                controlPoints.Count + ")!");
        }
        // End checks

        Destroy(startButton);
        startButton.enabled = false;
        Debug.Log("StartGame() called, starting game with " + playerConnections.Count + " players!");
        
        currentZone = 1;
        
        if (attackers.Count == 0 || defenders.Count == 0 || (attackers.Count != defenders.Count)) {
            // Here we are supposing that there are not enough players to start the game
            attackers.Clear();
            defenders.Clear();
            
            Random random = new Random();
            List<NetworkConnection> playerList = new List<NetworkConnection>(playerConnections);
            for (int i = 0; i < playerConnections.Count; i++) {
                NetworkConnection chosen = playerList[random.Next(playerList.Count)];
                playerList.Remove(chosen);

                CustomParameters parameters;

                if (customParameters.TryGetValue(chosen, out parameters) && parameters != null) {
                    int diff = attackers.Count - defenders.Count;
                    bool isAttacker = diff <= (playerConnections.Count - i-1) && parameters.team == "Attackers";
                }
                else {
                    bool isAttacker = i+1 <= Math.Ceiling(playerConnections.Count / 2f);
                    Debug.Log("Cekejecherche " + Math.Ceiling(playerConnections.Count / 2f));
                    GameObject playerObject = SpawnPlayer(isAttacker);

                    // If 5 players are playing, we want 3 attackers and 2 defenders
                    if (isAttacker) {
                        attackers.Add(chosen, playerObject);
                    }
                    else {
                        defenders.Add(chosen, playerObject);
                    }
                
                    NetworkServer.AddPlayerForConnection(chosen, playerObject);
                }
            }
        }
        else {
            // Here we suppose that there is enough players to start the game in both teams
            foreach (NetworkConnection attackerConnection in attackers.Keys) {
                GameObject playerObject = SpawnPlayer(true);
                NetworkServer.ReplacePlayerForConnection(attackerConnection, playerObject);
                attackers[attackerConnection] = playerObject;
            }
        }
    }

    public void NotifyPlayerDied(NetworkConnection connection) {
        // nameof returns the string associated with the function's name
        StartCoroutine(nameof(Respawn), connection);
    }

    private Transform GetSpawnPosition(bool isAttacker) {
        if (isAttacker)
            return spawnAttackers[currentZone - 1].transform;
        return spawnDefenders[currentZone - 1].transform;
    }

    private GameObject SpawnPlayer(bool isAttacker) {
        // Instantiate a player
        Transform startPos = GetSpawnPosition(isAttacker); // TODO: handle startpositions and use isAttacker + currentZone
        return Instantiate(playerPrefab, startPos.position + Vector3.up * 15, startPos.rotation);
    }

    private IEnumerator Respawn(NetworkConnection connection) {
        yield return new WaitForSeconds(respawnTime);
        
        bool isAttacker = attackers.ContainsKey(connection);
        GameObject playerObject = SpawnPlayer(isAttacker);

        NetworkServer.ReplacePlayerForConnection(connection, playerObject);
        if (isAttacker)
            attackers[connection] = playerObject;
        else
            defenders[connection] = playerObject;
    }
}