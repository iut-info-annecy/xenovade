﻿using UnityEngine;
using Mirror;

public class Grenade : NetworkBehaviour {
    public float delay = 3f;
    public GameObject explosionEffect;

    public float damage = 10f;
    public float radius = 10f;
    public float force = 700f;

    [SerializeField] AudioClip m_boum;
    AudioSource m_source;

    private float countdown;
    //[SyncVar]
    private bool hasExploded = false;

    // Start is called before the first frame update
    void Start() {
        countdown = delay;
        m_source = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update() {
        countdown -= Time.deltaTime;
        if (countdown <= 0f && !hasExploded) {
             Explode();
            //if (isServer)
                hasExploded = true;
        }
        if (hasExploded && !m_source.isPlaying)
        {
            Destroy(gameObject);
        }
    }

    public void Explode() {
        Instantiate(explosionEffect, transform.position, transform.rotation);
        m_source.clip = m_boum;
        m_source.volume = 0.75f;
        m_source.Play();

        Collider[] hitColliders = Physics.OverlapSphere(gameObject.transform.position, radius);

        foreach (Collider collider in hitColliders)
        {
            Target target = collider.GetComponent<Target>();
            Rigidbody rb = collider.GetComponent<Rigidbody>();
            Vector3 TossDirection = collider.transform.position+ Vector3.up - transform.position;
            float distance = TossDirection.magnitude;

            RaycastHit tir;
            bool qqChoseAuMilieu = Physics.Raycast(transform.position + Vector3.up, TossDirection, out tir);
            
            if (qqChoseAuMilieu)
            {
                Debug.Log(collider.transform.tag);
                Debug.Log(tir.transform.name);
                Debug.Log(tir.transform.tag);
                if (tir.transform.gameObject == collider.gameObject || tir.transform.tag == "Ground")
                {
                    qqChoseAuMilieu = false;
                }
            }

            if (isServer && target != null && !qqChoseAuMilieu)
            {
                TargetTakeDamage(collider.GetComponent<NetworkIdentity>().connectionToClient, target.gameObject,
                    damage * (1 - distance / radius) * (1 - distance / radius));
            }

            if (rb != null && !qqChoseAuMilieu)
            {
                if (collider.transform.gameObject.tag == "Player")
                {
                    ExplosionHandler explosionHandler = collider.gameObject.GetComponent<ExplosionHandler>();
                    explosionHandler.Explosion((TossDirection / distance) * (radius - distance), force / 200, (radius - distance) / radius);
                }
                else if (isServer)
                {
                    rb.AddExplosionForce(force, transform.position, radius);
                }
            }
        }
        if (isServer)
            transform.position = new Vector3(99999, 99999, 99999);
    }

    [TargetRpc]
    private void TargetTakeDamage(NetworkConnection target, GameObject player, float calculatedDamage) {
        player.GetComponent<Target>().TakeDamage(calculatedDamage);
    }
}