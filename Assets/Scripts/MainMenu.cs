﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour
{
    private Parameters parameters;
    public void Start()
    {
        parameters = GameObject.FindGameObjectWithTag("Parameters").GetComponent<Parameters>();
    }
    public void StartGame()
    {
        //initialise things about network
        SceneManager.LoadScene(1);
    }

    public void SetName()
    {
        GameObject nameInput = GameObject.FindGameObjectWithTag("Input");
        string name;
        name = nameInput.GetComponent<InputField>().text;
        if (name == null || name == "")
            name = "Guest";
        parameters.name = name;
    }

    public void ChooseAttack()
    {
        parameters.preferredTeam = Parameters.Team.Attackers;
    }
    public void ChooseDefense()
    {
        parameters.preferredTeam = Parameters.Team.Defenders;
    }

    public void ChooseHost()
    {
        parameters.chosenNetworkMode = Parameters.NetworkMode.Host;
    }

    public void ChooseHostPlay()
    {
        parameters.chosenNetworkMode = Parameters.NetworkMode.PlayerHost;
    }

    public void ChoosePlayer()
    {
        parameters.chosenNetworkMode = Parameters.NetworkMode.Player;
    }

    public void SetIP()
    {
        GameObject IPInput = GameObject.FindGameObjectWithTag("IP_Input");
        string IP;
        IP = IPInput.GetComponent<InputField>().text;
        if (IP == null || IP == "")
            IP = "Guest";
        parameters.IPAdress = IP;
    }

    public void Quit()
    {
        Application.Quit();
    }

}
