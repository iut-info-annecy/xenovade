﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CharacterController))]
public class ExplosionHandler : MonoBehaviour
{
    CharacterController characterController;
    private float explosionTimer = 1.0f;
    private Vector3 direction;
    private float force;

    // Start is called before the first frame update
    void Start()
    {
        characterController = GetComponent<CharacterController>();
    }

    void FixedUpdate()
    {
        if(explosionTimer > 0)
        {
            //characterController.Move();
            explosionTimer -= Time.deltaTime;
            transform.position = transform.position + (Time.deltaTime*explosionTimer* force * direction);
        }
    }

    public void Explosion(Vector3 pDirection, float pForce, float timer = 20.0f)
    {
        explosionTimer = timer;
        direction = pDirection;
        force = pForce;
    }
}
