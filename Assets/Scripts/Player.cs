﻿using UnityEngine;
using Mirror;
using UnityStandardAssets.Characters.FirstPerson;
using System.Collections.Generic;

public class Player : NetworkBehaviour {
    
    void Start() {
        if (!isLocalPlayer) {
            GetComponent<FirstPersonController>().enabled = false;
            GetComponent<AudioSource>().enabled = false;
            GetComponentInChildren<Camera>().enabled = false;
            foreach (Camera camera in GetComponentsInChildren<Camera>())
            {
                GameObject.Destroy(camera.gameObject);
            }
            GetComponentInChildren<AudioListener>().enabled = false;
            
            GetComponentInChildren<PlayerModel>().gameObject.layer = 0;
        }
        else {
            GetComponentInChildren<PlayerModel>().gameObject.layer = 8;
        }
    }
}