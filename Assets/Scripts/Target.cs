﻿using Mirror;
using UnityEngine;
using UnityEngine.UI;

public class Target : NetworkBehaviour {
    [SyncVar]
    public float health = 100f;

    private bool isDead = false;
    public bool IsDead => isDead;

    public void TakeDamage(float amount) {
        health -= amount;
        if (health <= 0f && !isDead) {
            isDead = true;
            Die();
        }

        if (isLocalPlayer) {
            GameObject gos;
            gos = GameObject.FindGameObjectWithTag("Health_ui");

            Text Health_text = gos.GetComponent<Text>();
            Health_text.text = health.ToString();
        }
    }
    [TargetRpc]
    public void TargetKnockback(NetworkConnection conn, float strength)
    {
        ExplosionHandler explosionHandler = GetComponent<ExplosionHandler>();
        explosionHandler.Explosion(-GetComponentInChildren<Camera>().transform.forward, strength, 0.2f);
    }
    void Die() {
        ClientScene.RemovePlayer();
    }
}