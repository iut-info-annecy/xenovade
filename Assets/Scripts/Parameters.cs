﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using Mirror;
using UnityEngine;
using Debug = UnityEngine.Debug;

public class Parameters : MonoBehaviour
{
    public enum Team { Attackers, Defenders }
    public Team preferredTeam;

    public enum NetworkMode { PlayerHost, Host, Player}
    public NetworkMode chosenNetworkMode;
    public string IPAdress;

    public string name;
    public void Start()
    {
        DontDestroyOnLoad(this);
    }

    public void Apply(XenoNetworkManager manager) {
        Debug.Log("Selected mode: " + chosenNetworkMode.ToString());
        manager.networkAddress = IPAdress;
        switch (chosenNetworkMode)
        {
            case NetworkMode.PlayerHost:
                manager.StartHost();
                break;
            case NetworkMode.Player:
                manager.StartClient();
                break;
            case NetworkMode.Host:
                manager.StartServer();
                break;
        }
    }
}
