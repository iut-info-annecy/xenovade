﻿using System;
using System.Collections;
using System.Collections.Generic;
using Mirror;
using UnityEngine;

public class Capturable : NetworkBehaviour {
    public float timeToCapture = 20f;
    [HideInInspector]
    public List<Collider> colliders;
    
    void Start() {
        colliders = new List<Collider>();
    }

    private void OnTriggerEnter(Collider other) {
        if (!isServer)
            return;
        
        if (!colliders.Contains(other))
            colliders.Add(other);
    }

    private void OnTriggerExit(Collider other) {
        if (!isServer)
            return;
        
        colliders.Remove(other);
    }
}