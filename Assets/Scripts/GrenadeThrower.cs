﻿using UnityEngine;
using Mirror;
using System.Collections.Generic;

public class GrenadeThrower : NetworkBehaviour {

    public float throwForce = 10f;
    public GameObject grenadePrefab;

    public GameObject grenadeUIPrefab;
    public List<GameObject> grenadeUI;

    public int initialAmmo = 10;

    private GameObject canvas;

    private int ammo;

    private void Start()
    {
        grenadeUI = new List<GameObject>();
        canvas = GameObject.FindGameObjectWithTag("BottomRight");

        Recharge();
    }

    void Update() {
        if (!isLocalPlayer)
            return;
        
        if (Input.GetButtonDown("Grenade") && ammo > 0) {
            ammo--;
            GameObject.Destroy(grenadeUI[ammo]);
            grenadeUI.Remove(grenadeUI[ammo]);
            CmdThrowGrenade(GetComponentInChildren<Camera>().transform.forward);
            Debug.Log("Grenade thrown !");
        }

        /*if (Input.GetButtonDown("Fire3"))
        {
            Recharge(2);
        }*/
    }
    
    private void OnDestroy()
    {
        if (isLocalPlayer)
            for (int i = grenadeUI.Count - 1; i >= 0; i--)
            { 
              GameObject.Destroy(grenadeUI[i]);
              grenadeUI.Remove(grenadeUI[i]);
            }
    }

    public void Recharge()
    {
        Recharge(initialAmmo);
    }
    public void Recharge(int value)
    {
        if (isLocalPlayer)
        {
            ammo += value;

            float posx = -20 - (grenadeUI.Count * 25);
            float posy = 25;

            for (int i = ammo - value; i < ammo; i++)
            {
                grenadeUI.Add(Instantiate(grenadeUIPrefab, new Vector3(posx, posy, 0), new Quaternion()));
                grenadeUI[i].transform.SetParent(canvas.transform, false);
                posx -= 25;
            }
        }
    }

    [Command]
    private void CmdThrowGrenade(Vector3 camForward) {
        GameObject grenade = Instantiate(grenadePrefab, transform.position + new Vector3(0, 1.5f, 0), transform.rotation);
        Rigidbody rb = grenade.GetComponent<Rigidbody>();
        rb.AddForce(camForward * throwForce, ForceMode.VelocityChange);
        NetworkServer.Spawn(grenade);
    }
}