﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

public class SelectionWeapon : MonoBehaviour
{
    public Animator animatorPistol;
    public Animator animatorRifle;
    public Animator animatorHeavy;
    public Animator animatorSniper;

    public void Update()
    {
        ActivateAll();
    }

    public void ActivateAll()
    {
        animatorPistol.SetBool("Pistol", true);
        animatorRifle.SetBool("Rifle", true);
        animatorHeavy.SetBool("Heavy", true);
        animatorSniper.SetBool("Sniper", true);
    }

    public void RotatePistol()
    {
        animatorPistol.SetBool("Pistol", true);
        Finish("Pistol");
    }
    public void RotateRifle()
    {
        animatorRifle.SetBool("Rifle", true);
        Finish("Rifle");
    }
    public void RotateHeavy()
    {
        animatorHeavy.SetBool("Heavy", true);
        Finish("Heavy");
    }
    public void RotateSniper()
    {
        animatorSniper.SetBool("Sniper", true);
        Finish("Sniper");
    }

    public void Finish(string name)
    {
        Debug.Log(name);
    }

}
