﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;
using UnityEngine.UI;

public class ClientGameManager : NetworkBehaviour
{
    public GameObject playerPrefab;
    public Text textProgress;
    public GameObject doorTop, doorBottom;
    public Text textTimeRemaining;
    public float timeRemaining = 10 * 60f;

    //public XenoNetworkManager manager;
    public ToNight toNight;
    public ToNightAreaLight toNightAreaLight;

    ///////////////////////////////////////
    
    private int currentZone = 1;

    private float timeToCapture;
    private float percentage;

    private float captureTimeLeft;
    private string captureProgressText = "capture";

    private void Awake()
    {
        captureProgressText = textProgress.text;
        textProgress.text = captureProgressText + " 0%";
    }

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (currentZone == 1)
        {
            doorTop.transform.localPosition = new Vector3(0, 19.72519f + 6.27481f * percentage, 0);
            doorBottom.transform.localPosition = new Vector3(0, -15 * percentage, 0);
        }
        else
        {
            doorTop.transform.localPosition = new Vector3(0, 19.72519f + 6.27481f, 0);
            doorBottom.transform.localPosition = new Vector3(0, -15 , 0);
        }
        Debug.Log("Code ultra moche à modifier quand y'aura le temps");
    }

    [ClientRpc]
    public void RpcSetTextProgress(string text)
    {
        textTimeRemaining.text = text;
    }

    [ClientRpc]
    public void RpcSwitchToNight()
    {
        toNight.shouldWait = true;
        toNightAreaLight.shouldWait = true;
    }

    [ClientRpc]
    public void RpcNextZone(float timeLeft)
    {
        currentZone++;
        captureTimeLeft = timeLeft;
    }

    [ClientRpc]
    public void RpcSetTimeToCaptureAndPercentage(float p_TimeToCapture, float p_Percentage)
    {
        float timeToCapture = p_TimeToCapture;
        float percentage = p_Percentage;
    }

    [ClientRpc]
    public void RpcUpdateUICaptureProgress(string newProgressText)
    {
        textProgress.text = newProgressText;
    }
}
